import requests
from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from converter.serializer import ConverterSerializer
from curencyconvertor.settings import CURRENCY_RATE_URL, API_KEY



class ConverterView(APIView):
    permission_classes = (AllowAny,)

    def post(self, request, format=None):
        # Add serializer validation
        serializer=ConverterSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        # Get data from serializer
        userdata = serializer.validated_data
        currency = userdata['currency']
        # Get data about rates
        response = requests.get(f'{CURRENCY_RATE_URL}?apikey={API_KEY}&base_currency={currency}')
        parsed_response = response.json()
        convert_to = userdata['convert_to']
        # Fetch rate from response
        cof = parsed_response["data"][convert_to]
        value = userdata['value']
        # Build response
        return Response(status=status.HTTP_200_OK, data={
            "curency": convert_to,
            "value": cof * value,
            "conversion rate": cof
        })
