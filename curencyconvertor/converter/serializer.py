from rest_framework import serializers


class ConverterSerializer(serializers.Serializer):
    currency = serializers.ChoiceField(choices=['USD', 'EUR', 'GBP', 'UAH'])
    value = serializers.FloatField(min_value=0)
    convert_to = serializers.ChoiceField(choices=['USD', 'EUR', 'GBP', 'UAH'])

    # Override is_valid method
    def is_valid(self, raise_exception=False):
        is_valid = super().is_valid(raise_exception)
        # Check is currency and convert_to equal
        if self.validated_data["currency"] == self.validated_data["convert_to"]:
            raise serializers.ValidationError("fields \"currency\" and \"convert_to\"cannot match")
        return is_valid
