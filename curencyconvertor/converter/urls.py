from django.urls import path

from converter.views import ConverterView

urlpatterns=[path('currencyconverter/',ConverterView.as_view())]